from pydantic import BaseModel
from enum import Enum


class SexDef(str, Enum):
    male = "male"
    female = "female"


class Passenger(BaseModel):
    PassengerId: int
    Pclass: int
    Name: str
    Sex: SexDef
    Age: float
    SibSp: int
    Parch: int
    Ticket: str
    Fare: float
    Cabin: str
    Embarked: str

    class Config:
        use_enum_values = True  # the enum field values will be converted to string
