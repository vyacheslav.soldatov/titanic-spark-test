from pydantic import BaseModel

from Passenger import SexDef


class PredictOutput(BaseModel):
    PassengerId: int
    Pclass: int
    Name: str
    Sex: SexDef
    Age: float
    SibSp: int
    Parch: int
    Ticket: str
    Fare: float
    Cabin: str
    Embarked: str
    Predict: float

    class Config:
        use_enum_values = True  # the enum field values will be converted to string

    def fill_values_from_basemodel(self, values: dict):
        for field_name in values:
            if field_name in self.__dict__:
                self.__dict__[field_name] = values[field_name]
