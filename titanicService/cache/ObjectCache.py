class ObjectCache:
    def __init__(self, single_value_per_key: bool = False):
        self.store_single = single_value_per_key
        self.__cache = {}

    def save(self, key: str, value):
        if key not in self.__cache:
            self.__cache[key] = value
        else:
            if not self.store_single:
                current_value = self.__cache[key]
                if isinstance(current_value, list):
                    self.__cache[key].append(value)
                else:
                    new_value = [current_value, value]
                    self.__cache[key] = new_value

    def remove(self, key):
        if key in self.__cache:
            self.__cache.pop(key)
