from pyspark.sql import DataFrame
from pyspark.sql.types import IntegerType, DoubleType
import pyspark.sql.functions as spark_func
from model.input.validation.validators import is_positive_float, is_posittive_integer, is_nonempty_string


def calc_sex_male_feature(df_in: DataFrame) -> DataFrame:
    return df_in.withColumn(
                'sex_male',
                spark_func.when((spark_func.col("Sex") == 'male'), 1).otherwise(0)
            )


def calc_family_size_feature(df_in: DataFrame) -> DataFrame:
    return df_in.withColumn(
                'FamilySize',
                spark_func.expr("SibSp + Parch + 1")
            )


def calc_is_alone_feature(df_in: DataFrame) -> DataFrame:
    return df_in.withColumn(
                'IsAlone',
                spark_func.expr("FamilySize == 1").cast('integer')
            )


def calc_age_multiply_w_class_feature(df_in: DataFrame) -> DataFrame:
    return df_in.withColumn(
                'Age*Class',
                spark_func.expr("Age * Pclass")
            )


def calc_embarked_feature(df_in: DataFrame) -> DataFrame:
    df_out = df_in.replace(
                            to_replace={
                                        'S': '0',
                                        'C': '1',
                                        'Q': '2'
                            },
                            subset=['Embarked']
                        )
    df_out = df_out.withColumn("Embarked", df_out["Embarked"].cast(IntegerType()))
    return df_out


model_config = {
    'model_name': 'titanic',
    'path_to_model': 'random_forest_.sav',

    'input_fields_with_defaults': {
                                        'PassengerId': -1,
                                        'Pclass': -1,
                                        'Name': '',
                                        'Sex': '',
                                        'Age': -1,
                                        'SibSp': -1,
                                        'Parch': -1,
                                        'Ticket': '',
                                        'Fare': 0.0,
                                        'Cabin': '',
                                        'Embarked': ''
                                    },

    # constraints for the incoming JSON strings
    'validation_checks': {
                            'PassengerId': is_posittive_integer,
                            'Pclass': is_posittive_integer,
                            'Name': is_nonempty_string,
                            'Sex': is_nonempty_string,
                            'Age': is_positive_float,
                            'SibSp': is_posittive_integer,
                            'Parch': is_posittive_integer,
                            'Ticket': is_nonempty_string,
                            'Fare': is_positive_float,
                            'Cabin': is_nonempty_string,
                            'Embarked': is_nonempty_string
                        },

    # data types of columns in Spark dataframe
    'spark_fields_casts': {
                            "PassengerId": IntegerType,
                            "Pclass": IntegerType,
                            "Age": DoubleType,
                            "SibSp": IntegerType,
                            "Parch": IntegerType,
                            "Fare": DoubleType
                        },

    'fillna': {
                    'Age': 27.915,  # avg age
                    'Embarked': 'S'
                },

    'columns_to_drop': ['Parch', 'SibSp', 'FamilySize', 'Sex', 'Name', 'Cabin', 'Ticket'],

    # Dictionary order is guaranteed to be insertion order since Python 3.7
    # so it may be useful to build the feature calculation as a set of instructions
    # executing one after another for the columns identified by feature name as key
    # TODO: not implemented yet
    'features_to_calc': {
        'sex_male': calc_sex_male_feature,
        'FamilySize': calc_family_size_feature,
        'IsAlone': calc_is_alone_feature,
        'Age*Class': calc_age_multiply_w_class_feature,
        'Embarked': calc_embarked_feature
    },

    'all_features': [
                    'Pclass',
                    'Age',
                    'Fare',
                    'Embarked',
                    'sex_male',
                    'IsAlone',
                    'Age*Class'
                ],

    'features_col_name': 'features',
    'predict_col_name': 'PREDICT'
}









