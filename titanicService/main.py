from pyspark.sql.functions import udf
from pyspark.sql.types import FloatType
import uuid

from PredictOutput import PredictOutput
from model \
    import \
    build_model_input, \
    decode_input, \
    validate_input, \
    describe_fails, \
    init_model, ModelInput

from configuration import model_config

from model.execution.DataProcessing \
    import \
    cast_input_fields, \
    drop_unused_columns, \
    fillna_values, \
    calc_feature, \
    add_feature_list_column, \
    calc_predict, \
    DataProxy, \
    ModelProxy

import os
import sys

from typing import Optional
from fastapi import FastAPI, APIRouter, Header, Response
#from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.cors import CORSMiddleware
import uvicorn

# this stuff is needed for launching python3 from JVM
# which earlier could crash doing it being unaware of virtual environment details
from Passenger import Passenger

os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable

router = APIRouter()

# add the origins to allow for communicating with our service
origins = [
    "null",
    "http://127.0.0.1",
    "https://127.0.0.1",
    "http://localhost",
    "http://localhost:8000",
]


def create_app(app_router: APIRouter, app_origins: list) -> CORSMiddleware:
    fastapi_app = FastAPI()
    fastapi_app.include_router(app_router)
    return CORSMiddleware(
        fastapi_app,
        allow_origins=app_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@router.get('/')
async def root():
    return {'message': 'Hi there'}


async def mock_request(
        rq_id: str,
        model_input: ModelInput
) -> float:
    if not rq_id:
        rq_id = str(uuid.uuid4())
    print(rq_id)

    # mocking the service call:
    # inputJson = '{"PassengerId":35,"Pclass": 2,"Name": "Jimmy","Sex": "male","Age": 34.0,"SibSp": 0,"Parch": 0,"Ticket": "12332","Fare": 71.2833,"Cabin": "C85","Embarked": "S"}'
    # model_input = decode_input(inputJson, model_config['input_fields_with_defaults'])

    input_is_valid, input_status = validate_input(model_input, model_config['validation_checks'])

    if not input_is_valid:
        print(describe_fails(input_status))
    else:
        input_data_dict = model_input.__dict__

        # validate input first
        spark_df = model.parse_input(input_data_dict)

        spark_df = cast_input_fields(spark_df, model_config['spark_fields_casts'])

        # fill NA
        spark_df = fillna_values(spark_df, model_config['fillna'])

        # calc features
        for feature in model_config['features_to_calc']:
            spark_df = calc_feature(
                spark_df,
                model_config['features_to_calc'],
                feature
            )

        # drop unused columns
        spark_df = drop_unused_columns(spark_df, model_config['columns_to_drop'])

        # add features list
        spark_df = add_feature_list_column(
            spark_df,
            model_config['all_features'],
            model_config['features_col_name']
        )

        # calculate the prediction
        result_df = calc_predict(spark_df, model_config['predict_col_name'], predict_udf)
        pd_df = result_df.toPandas()

        predict_value = pd_df.iloc[0][model_config['predict_col_name']]

        mock_result = "predict is: " + str(predict_value)

        print(mock_result)
        return predict_value


@router.post('/mock', response_model=PredictOutput)
async def mock_rq(
        incoming_rq: Passenger,
        response: Response,
        RequestID: Optional[str] = Header(None)
) -> PredictOutput:

    model_input = build_model_input(incoming_rq.dict())
    mock_rs = await mock_request(RequestID, model_input)
    rs_body = PredictOutput.construct(incoming_rq.__fields_set__, **incoming_rq.dict())
    rs_body.Predict = mock_rs
    response.headers["RequestID"] = RequestID
    return rs_body


if __name__ == '__main__':

    # init stage start
    # loading model to Spark
    model = init_model(model_config['path_to_model'])
    # initializing proxies
    model_proxy = ModelProxy()
    data_proxy = DataProxy(model_config['model_name'], model_proxy)
    model_proxy.add_model(model_config['model_name'], model.bc_model_state.value)

    # let's register our udf when we already have SparkContext initialized
    predict_udf = udf(data_proxy.calc_predict_udf, FloatType())

    # init stage completed

    # we start the FastAPI application here
    app = create_app(router, origins)
    # we can run the server to start receiving the requests
    uvicorn.run(app, host="127.0.0.1", port=8000)

    # for the graceful stop
    # let's give the spark 'cluster' some time to calm down
    # time.sleep(3)
    #
    # print("that's all, folks")
