from model.input.ModelInput import ModelInput as ModelInput
from model.input.validation.input_validation import input_is_valid as input_is_valid
from model.input.validation.input_validation import describe_validation_fails as describe_fails
from model.execution.SparkModelWrapper import SparkModelWrapper as ModelWrapper


def build_model_input(input_fields: dict):
    return ModelInput(input_fields)


def decode_input(json_string: str, input_structure: dict):
    data = ModelInput(input_structure)
    return data.from_json(json_string)


def validate_input(data_in: ModelInput, validation_checks: dict):
    return input_is_valid(data_in, validation_checks)


def init_model(path_to_model: str):
    model = ModelWrapper(path_to_model)
    return model


