import numpy as np
import pandas as pd
from pyspark.sql import DataFrame
import pyspark.sql.functions as spark_func
import pyspark.sql.types as spark_types

from \
    pyspark.sql.types \
    import \
    IntegerType, \
    DoubleType, \
    StringType, \
    StructField, \
    StructType

to_spark_types_mapping = {
    int: IntegerType,
    float: DoubleType,
    str: StringType
}


def generate_data_schema(data_fields_types: dict, input_data: dict):
    schema_fields = []

    for field in input_data:
        field_type = None
        # use the mapping provided
        if field in data_fields_types:
            field_type = data_fields_types[field]
        if field_type is None:
            # guess the type when the mapping was not defined
            field_value_type = type(input_data[field])
            if field_value_type in to_spark_types_mapping:
                field_type = to_spark_types_mapping[field_value_type]
        schema_fields.append(
            StructField(field, field_type(), True)
        )
    data_schema = StructType(schema_fields)
    return data_schema


def cast_input_fields(input_df: DataFrame, fields_mapping: dict) -> DataFrame:
    # manipulating with same input object here,
    # but we should use input_df.alias("out_df") first, to obtain a new object
    # in case we want to preserve an original parsed input object untouched
    for field_name in fields_mapping:
        input_df = input_df.withColumn(field_name, input_df[field_name].cast(fields_mapping[field_name]()))

    return input_df


def fillna_values(input_df: DataFrame, defaults: dict) -> DataFrame:
    output_df = input_df  # initiate a link to the dataframe
    for field_name in defaults:
        # altering the dataframe returns new one, the original remains in same state
        output_df = output_df.fillna(
            value=defaults[field_name],
            subset=[field_name]
        )
    return output_df


def drop_unused_columns(input_df: DataFrame, column_names: list) -> DataFrame:
    output_df = input_df.drop(*column_names)
    return output_df


def calc_feature(
        input_df: DataFrame,
        features_calculations: dict,
        feature_name: str
) -> DataFrame:
    # bypass the input_df in case there any
    if feature_name in features_calculations:
        input_df = features_calculations[feature_name](input_df)
    return input_df


def add_feature_list_column(
        df_in: DataFrame,
        features_list: list,
        features_list_col_name: str
) -> DataFrame:
    df_out = df_in.withColumn(
        features_list_col_name,
        spark_func.array(
            [
                spark_func.col(f"{col}") for col in features_list
            ]
        )
    )
    df_out = df_out.withColumn(
        features_list_col_name,
        df_out[features_list_col_name].cast(
            spark_types.ArrayType(spark_types.StringType())
        )
    )
    return df_out


def calc_predict(
        input_df: DataFrame,
        predict_column_name: str,
        predict_udf: type(lambda x: None)
) -> DataFrame:
    return input_df.withColumn(
        predict_column_name,
        predict_udf(input_df['features'])
    )


# this is a sort of a storage for the links to loaded models
# which is used by the UDFs to use them being transferred into Spark workers
# (storing the links inside them is prohibited and would not pass the pickling stage)
class ModelProxy:
    def __init__(self):
        self.__models = {}

    # should be called after the depending DataProxy is initialized
    # and its calc_predict_udf method is registered as UDF
    def add_model(self, model_name: str, model):
        self.__models[model_name] = model

    def get_model_by_name(self, model_name):
        return self.__models[model_name]


# a sort of a proxy which helps instantiate a UDF
# which needs a link to the model which was loaded to Spark cluster
class DataProxy:
    def __init__(self, model_name: str, model_proxy: ModelProxy):
        self.__model_ref = None
        self.__name = model_name
        self.__models_proxy = model_proxy
        pass

    def calc_predict_udf(self, features: pd.Series) -> float:
        np_features = np.array(features).reshape(1, -1)
        model = self.__models_proxy.get_model_by_name(self.__name)
        prediction = model.predict_proba(np_features)[:, 1]
        return prediction.item()
