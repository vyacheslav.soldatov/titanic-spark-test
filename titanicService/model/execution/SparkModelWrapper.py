import pickle
import numpy as np
import pandas as pd
from pyspark.sql import SparkSession
from pyspark.context import SparkContext
from pyspark.sql import Row, DataFrame
from model.execution.DataProcessing import generate_data_schema


class SparkModelWrapper:

    def __init__(self, path_to_model: str):
        # init Spark stuff
        self.__sc = SparkContext('local', 'test')
        self.__spark = SparkSession.builder.master("local[2]").getOrCreate()

        # load model from pickle
        self.loaded_model = pickle.load(open(path_to_model, 'rb'))
        self.bc_model_state = self.__spark.sparkContext.broadcast(self.loaded_model)

        print('Spark Model Wrapper init completed')

    def parse_input(self, input_data: dict):
        # data_schema = generate_data_schema(data_fields_types, input_data)

        # data_values = Row(*input_data.values())

        # rdd = self.__sc.parallelize(data_values)

        # input_df = self.__spark.createDataFrame(
        #     data=rdd,
        #     schema=data_schema
        # )

        # the best working way is surprisingly straightforward:
        # just pass the dictionary as an only element of a list
        # and the library will be able to accept it,
        # also it builds the schema it can properly work with
        # (the schema defined manually does not work for some reason)
        input_df = self.__spark.createDataFrame([input_data])
        return input_df
