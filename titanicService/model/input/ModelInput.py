import json


class ModelInput:

    def __init__(self, input_fields: dict):
        # setup the input fields and default values
        for filed_name in input_fields:
            self.__dict__[filed_name] = input_fields[filed_name]

    def from_json(self, json_string):
        fields = json.loads(json_string)
        input_attributes = self.__dict__
        # we ensure to take only those fields from JSON
        # which names correspond to the class fields,
        # others will be lost
        for attr in input_attributes:
            if attr in fields:
                self.__dict__[attr] = fields[attr]
        return self
