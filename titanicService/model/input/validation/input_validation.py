from model.input.ModelInput import ModelInput


def input_is_valid(input_data: ModelInput, validation_checks: dict):
    checks_state = {}
    validation_result = True
    input_fields = input_data.__dict__

    for field_name in validation_checks:
        if field_name in input_fields:
            field_status, status_desc = validation_checks[field_name](
                                                                        input_fields[field_name]
                                                            )
            if not field_status:
                validation_result = False
                # may also use 'str.format', but this way looks a bit more interesting
                checks_state[field_name] = ''.join([
                                                        field_name,
                                                        ' ',
                                                        status_desc
                                                    ])

    return validation_result, checks_state


def describe_validation_fails(validations_fails: dict):
    fail_description = 'Validation fail reason(s):'
    for index, reason in enumerate(validations_fails):
        # may also use 'str.format', but this way looks a bit more interesting
        fail_description = ''.join([
                                fail_description,
                                ' ',
                                validations_fails[reason],
                                ',' if index < len(validations_fails.items())-1 else '.'
                            ])
    return fail_description

