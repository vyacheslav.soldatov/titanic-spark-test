def is_posittive_integer(test_value):
    check_result = False
    not_an_int_reason = 'is not an integer'
    less_zero_reason = 'value is less than zero'
    check_status = 'OK'

    if isinstance(test_value, int):
        if test_value >= 0:
            check_result = True
        else:
            check_status = less_zero_reason
    else:
        check_status = not_an_int_reason

    return check_result, check_status


def is_nonempty_string(test_value):
    check_result = False
    not_a_string_reason = 'is not a string'
    empty_string_value = 'is empty string'
    check_status = 'OK'

    if isinstance(test_value, str):
        if not test_value:
            check_status = empty_string_value
        else:
            check_result = True
    else:
        check_status = not_a_string_reason

    return check_result, check_status


def is_positive_float(test_value):
    check_result = False
    not_a_float_reason = 'is not a float'
    zero_or_negative_reason = 'is less or equals zero'
    check_status = 'OK'

    if isinstance(test_value, float):
        if test_value >= 0:
            check_result = True
        else:
            check_status = zero_or_negative_reason
    else:
        check_status = not_a_float_reason

    return check_result, check_status
